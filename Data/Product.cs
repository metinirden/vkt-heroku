﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace vkt_heroku.Data
{
    public partial class Product
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public double? Price { get; set; }
        public ICollection<ProductCategory> ProductCategory { get; set; }
    }
}
