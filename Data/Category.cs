﻿using System;
using System.Collections.Generic;

namespace vkt_heroku.Data
{
    public partial class Category
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public ICollection<ProductCategory> ProductCategory { get; set; }
    }
}
