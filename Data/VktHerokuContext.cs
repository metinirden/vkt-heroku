﻿using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace vkt_heroku.Data
{
    public class VktHerokuContext : DbContext
    {
        public VktHerokuContext(DbContextOptions<VktHerokuContext> options) : base(options) { }
        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<ProductCategory> ProductCategory { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>(entity =>
            {
                entity.HasKey(i => i.ID);
                entity.Property(e => e.Name).IsRequired();
            });
            modelBuilder.Entity<Product>(entity =>
            {
                entity.HasKey(i => i.ID);
                entity.Property(e => e.Name).IsRequired();
            });
            modelBuilder.Entity<ProductCategory>(entity =>
            {
                entity.HasKey(i => i.ID);
                entity.Property(e => e.CategoryID).HasColumnName("CategoryID");

                entity.Property(e => e.ProductID).HasColumnName("ProductID");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.ProductCategory)
                    .HasForeignKey(d => d.CategoryID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Category_ProductCategory");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.ProductCategory)
                    .HasForeignKey(d => d.ProductID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Product_ProductCategory");
            });
        }
    }
}