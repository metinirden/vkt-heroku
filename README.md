# vkt-heroku
.Net Core, Heroku ve Postgres altyapısı ile denemeler.

[Demo Linki](https://vkt-heroku.herokuapp.com/)

# Amaç

Bu repo'da bulunan kodların amacı, kullanılan uygulama altyapıların (.net core, heroku, mvc core, postgresql) detaylarına inmek yerine birlikte uyumlu çalışmalarını test etmek ve gözlemlemektedir. Heroku'nun detayına inme durum var biraz, oda deployment ve pipeline ile ilgili avantaj ve kısayolları keşfetmek içindir.

# Kullanılan paketler
* BundlerMinifier.Core (Bundling ve Minifying için)
* Microsoft.EntityFrameworkCore.Design
* Microsoft.EntityFrameworkCore.Tools
* Npgsql.EntityFrameworkCore.PostgreSQL (EF Postgres entegrasyonu için)

# Kullanılan CLI tool'ları
* Microsoft.VisualStudio.Web.CodeGeneration.Tools
* BundlerMinifier.Core (Bundle ve Minifying command line tool'u)
* Microsoft.EntityFrameworkCore.Tools.DotNet (EF Scaffolding ve migration cli tool'u)

# Uygulama Özeti
* Db ve Orm yapısı
    * Heroku Postgres db üzerinde ilişkisel bir db yaklaşımı kullanılıyor.
    * Db'ye bağlanmak için EF'nin Postgres entegrasyonu kullanılıyor.
    * Postgres'den dolayı önceki alışkanlıklarımıza ters olacak bir durum yok, fakat EntityFramework Core'ın gelişim süreci devam ettiğinden dolayı lazy loading, many-to-many relationlarda farklılıklar mevcut.
    * Config ayarları, .Net Core platform'unda web.config yerine, appsetttings.json dosyasında yer almakta. 
        * Örnek olması adına Product, Category ve bunları çoka çok olarak bağlayan ProductCategory tabloları ve bunların listelenmesi ve eklenmesi ile ilgi sayfalar yapıldı.(Demo link'inden görüntülenebilir.)
    * Db-Class ilişkisini kurmak için, ef cli tool'unun migration ya da scaffolding yapısı kullanılabilir. Fakat bizim daha yakın olduğumuz dbFirst(scaffolding) yapısı tam gelişmemiş ya da postgre ile entegrasyonunda sorun var gibi. Örnek olarak primary keyField'ları codebase'e çekerken, key olarak işaretlemiyor. 


* Mimari yapı
    * .Net Core ile gelen yeni mimari yaklaşımlar sonucunda, dependency injection ile istenilen servis, dbcontext, helper class'ları alabiliyoruz.
    * Uygulama eskisinin aksine IIS'de çalışmak zorunda değil, bir console app gibi Program.cs dosyası ile Kester adlı ufak bir arayüz ile çalışıyor. Bunu mevcut apache, iis gibi http server'lar ile bağlayabilmekteyiz.

* Heroku ve Deployment
    * Proje, bitbucket üzerine git ile bağlı durumda.
    * Heroku'ya deployment için, git'i heroku'nun kendi git'ine bağlayarak her bir committe otomatik deployment yapılabilmektedir.
    * Avantaj olarak versiyon rollback gibi şeylere, pipeline ile test-stage-production gibi farklı ortamlara ve aralarında hızlı geçişlere izin vermektedir.

Son Düzenleme: Çarşamba 09/05/2018