using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using vkt_heroku.Data;

namespace vkt_heroku.Controllers
{
    public class ProductController : Controller
    {
        private VktHerokuContext ctx;

        public ProductController(VktHerokuContext context)
        {
            ctx = context;
        }

        public IActionResult Index()
        {
            var prods = ctx.Product.ToList();
            return View(prods);
        }

        public IActionResult Add()
        {
            ViewData["Categories"] = ctx.Category.ToList();
            return View();
        }

        [HttpPost]
        public IActionResult Add([FromForm()]ProductCategory productCategory)
        {
            if (ModelState.IsValid)
            {
                ctx.Product.Add(productCategory.Product);
                if (productCategory.CategoryID != 0)
                    ctx.ProductCategory.Add(productCategory);
                ctx.SaveChanges();
            }
            return RedirectToAction("Index");
        }
    }
}
