using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using vkt_heroku.Data;

namespace vkt_heroku.Controllers
{
    public class CategoryController : Controller
    {
        private VktHerokuContext ctx;

        public CategoryController(VktHerokuContext context)
        {
            ctx = context;
        }

        public IActionResult Index()
        {
            var cats = ctx.Category.Include(i => i.ProductCategory).ThenInclude(f => f.Product).ToList();
            return View(cats);
        }

        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Add([Bind("Name")]Category category)
        {
            if (ModelState.IsValid)
            {
                ctx.Category.Add(category);
                ctx.SaveChanges();
            }
            return RedirectToAction("Index");
        }
    }
}
